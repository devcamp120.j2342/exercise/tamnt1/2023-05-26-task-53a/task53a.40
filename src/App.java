
public class App {
    public static void main(String[] args) throws Exception {

        InvoiceItem invoiceItem1 = new InvoiceItem(1, "best chair for gaming", 2, 20000);
        InvoiceItem invoiceItem2 = new InvoiceItem(2, "best table for dining", 8, 40000);
        System.out.println("Sản phẩm " + invoiceItem1.toString() + "với tổng giá trị:" + invoiceItem1.getTotal());
        System.out.println("Sản phẩm " + invoiceItem2.toString() + "với tổng giá trị:" + invoiceItem2.getTotal());

    }

}